﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Project_Prototype_v2.Models
{
    public class ProjectModel : ProjectsModel, INotifyPropertyChanged
    {
        private long _id;
        private string _title;
        private string _slug;
        private string _description;
        private DateTime _deadline;
        private int _userId;
        private DateTime? _createdAt;
        private DateTime? _updatedAt;

        [JsonProperty("id")]
        public long Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("title")]
        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("slug")]
        public string Slug
        {
            get { return _slug; }
            set
            {
                _slug = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("description")]
        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("deadline")]
        public DateTime Deadline
        {
            get { return _deadline; }
            set
            {
                _deadline = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("user_id")]
        public int UserId
        {
            get { return _userId; }
            set
            {
                _userId = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("created_at")]
        public DateTime? CreatedAt
        {
            get { return _createdAt; }
            set
            {
                _createdAt = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("updated_at")]
        public DateTime? UpdatedAt
        {
            get { return _updatedAt; }
            set
            {
                _updatedAt = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

    }
}
