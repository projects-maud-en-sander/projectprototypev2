﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Project_Prototype_v2.Models
{
    public class BacklogItemModel : BacklogItemsModel, INotifyPropertyChanged
    {
        private long _id;
        private string _userRole;
        private string _action;
        private int _storyPoints;
        private DateTime? _createdAt;
        private DateTime? _updatedAt;
        private int _projectId;
        private int? _userId;
        private int? _sprintId;
        private int _stateId;
        private int _backlogTypeId;
        private BacklogTypeModel _backlogType;

        public string Als
        {
            get { return "Als"; }
        }

        public string WilIk
        {
            get { return "wil ik"; }
        }

        [JsonProperty("id")]
        public long Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("user_role")]
        public string UserRole
        {
            get
            {
                return _userRole;
            }
            set
            {
                _userRole = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("action")]
        public string Action
        {
            get
            {
                return _action;
            }
            set
            {
                _action = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("story_points")]
        public int StoryPoints
        {
            get
            {
                return _storyPoints;
            }
            set
            {
                _storyPoints = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("created_at")]
        public DateTime? CreatedAt
        {
            get
            {
                return _createdAt;
            }
            set
            {
                _createdAt = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("updated_at")]
        public DateTime? UpdatedAt
        {
            get
            {
                return _updatedAt;
            }
            set
            {
                _updatedAt = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("project_id")]
        public int ProjectId
        {
            get
            {
                return _projectId;
            }
            set
            {
                _projectId = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("user_id")]
        public int? UserId
        {
            get
            {
                return _userId;
            }
            set
            {
                _userId = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("sprint_id")]
        public int? SprintId
        {
            get
            {
                return _sprintId;
            }
            set
            {
                _sprintId = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("state_id")]
        public int StateId
        {
            get
            {
                return _stateId;
            }
            set
            {
                _stateId = value;
                OnPropertyChanged();
            }
        }



        [JsonProperty("backlog_type_id")]
        public int BacklogTypeId
        {
            get
            {
                return _backlogTypeId;
            }
            set
            {
                _backlogTypeId = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("backlog_type")]
        public BacklogTypeModel BacklogType
        {
            get { return _backlogType; }
            set
            {
                _backlogType = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
