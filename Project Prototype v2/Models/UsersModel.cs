﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Prototype_v2.Models
{
    public class UsersModel
    {
        [JsonProperty("Users")]
        public UserModel[] Users { get; set; }
    }
}
