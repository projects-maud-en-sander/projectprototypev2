﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Prototype_v2.Models
{
    public class SprintsModel
    {
        [JsonProperty("Sprints")]
        public SprintModel[] Sprints { get; set; }

    }
}
