﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Project_Prototype_v2.Models
{
    public class SprintModel : INotifyPropertyChanged
    {
        private long _id;
        private string _name;
        private string _slug;
        private int _setup;
        private DateTime _startDate;
        private DateTime _endDate;
        private long _projectId;
        private DateTime? _createdAt;
        private DateTime? _updatedAt;

        [JsonProperty("id")]
        public long Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("name")]
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("slug")]
        public string Slug
        {
            get { return _slug; }
            set
            {
                _slug = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("setup")]
        public int Setup
        {
            get { return _setup; }
            set
            {
                _setup = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("start_date")]
        public DateTime StartDate
        {
            get { return _startDate; }
            set
            {
                _startDate = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("end_date")]
        public DateTime EndDate
        {
            get { return _endDate; }
            set
            {
                _endDate = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("project_id")]
        public long ProjectId
        {
            get
            {
                return _projectId;
            }
            set
            {
                _projectId = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("created_at")]
        public DateTime? CreatedAt
        {
            get { return _createdAt; }
            set
            {
                _createdAt = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("updated_at")]
        public DateTime? UpdatedAt
        {
            get { return _updatedAt; }
            set
            {
                _updatedAt = value;
                OnPropertyChanged();
            }
        }

        public SprintsModel SprintsModel
        {
            get => default;
            set
            {
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
