﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Project_Prototype_v2.Models
{
    public class BacklogTypeModel : BacklogTypesModel, INotifyPropertyChanged
    {
        private long _id;
        private string _name;
        private string _slug;
        private int _enabled;
        private DateTime? _createdAt;
        private DateTime? _updatedAt;

        [JsonProperty("id")]
        public long Id
        {
            get { return _id; }
            set
            {
                _id = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("name")]
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("slug")]
        public string Slug
        {
            get { return _slug; }
            set
            {
                _slug = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("enabled")]
        public int Enabled
        {
            get { return _enabled; }
            set
            {
                _enabled = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("created_at")]
        public DateTime? CreatedAt
        {
            get { return _createdAt; }
            set
            {
                _createdAt = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("updated_at")]
        public DateTime? UpdatedAt
        {
            get { return _updatedAt; }
            set
            {
                _updatedAt = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
