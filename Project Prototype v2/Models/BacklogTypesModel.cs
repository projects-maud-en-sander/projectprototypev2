﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Project_Prototype_v2.Models
{
    public class BacklogTypesModel
    {
        [JsonProperty("BacklogTypes")]
        public BacklogTypeModel[] BacklogTypes { get; set; }
    }
}
