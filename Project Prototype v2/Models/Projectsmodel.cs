﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Prototype_v2.Models
{
    public class ProjectsModel
    {
        [JsonProperty("Projects")]
        public ProjectModel[] Projects { get; set; }

        [JsonProperty("OwnProjects")]
        public ProjectModel[] OwnProjects { get; set; }
    }
}
