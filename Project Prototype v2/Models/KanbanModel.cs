﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Project_Prototype_v2.Models
{
    public class KanbanModel : KanbansModel
    {
        [JsonProperty("backlog")]
        public BacklogItemModel[] Backlog { get; set; }

        [JsonProperty("in_progress")]
        public BacklogItemModel[] InProgress { get; set; }

        [JsonProperty("done")]
        public BacklogItemModel[] Done { get; set; }
    }
}
