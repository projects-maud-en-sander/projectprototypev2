﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Project_Prototype_v2.Models
{
    public class UserModel : UsersModel
    {
        private long _id;
        private string _name;
        private string _email;
        private string _emailVerifiedAt;
        private DateTime? _createdAt;
        private DateTime? _updatedAt;

        [JsonProperty("id")]
        public long Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("name")]
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("email")]
        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                _email = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("email_verified_at")]
        public string EmailVerifiedAt
        {
            get
            {
                return _emailVerifiedAt;
            }
            set
            {
                _emailVerifiedAt = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("created_at")]
        public DateTime? CreatedAt
        {
            get { return _createdAt; }
            set
            {
                _createdAt = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("updated_at")]
        public DateTime? UpdatedAt
        {
            get { return _updatedAt; }
            set
            {
                _updatedAt = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

    }
}
