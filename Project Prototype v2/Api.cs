﻿using Newtonsoft.Json;
using Project_Prototype_v2.Models;
using Project_Prototype_v2.Views;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Project_Prototype_v2
{
    class Api
    {
        public SignInModel _signinmodel;

        public Api()
        {
            var JsonDate = JsonConvert.SerializeObject(
                new

                {
                    email = LoginView.email,
                    password = LoginView.password
                });
            var httpContent = new ByteArrayContent(Encoding.UTF8.GetBytes(JsonDate));
            httpContent.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");
            _signinmodel = JsonConvert.DeserializeObject<SignInModel>(ApiCall("/api/v2/auth/create", httpContent, MethodType.post));


        }



        public async Task<HttpResponseMessage> PatchAsync(HttpClient client, string uri, HttpContent iContent)
        {
            try
            {
            var method = new HttpMethod("PATCH");

            Uri requestUri = new Uri(client.BaseAddress + uri);

            var request = new HttpRequestMessage(method, requestUri)
            {
                Content = iContent
            };

            HttpResponseMessage response = new HttpResponseMessage();
                // In case you want to set a timeout
                //CancellationToken cancellationToken = new CancellationTokenSource(60).Token;
                TimeSpan ts = new TimeSpan(0, 0, 10);
                client.Timeout = ts;
               // httpClient.SendAsync(httpContent).ConfigureAwait(false);
                response = await client.SendAsync(request).ConfigureAwait(false);
                // If you want to use the timeout you set
                //response = await client.SendRequestAsync(request).AsTask(cancellationToken);
                return response;
            }
            catch (TaskCanceledException e)
            {
                Debug.WriteLine("ERROR: " + e.ToString());
                return null;
            }

            
        }

        public string ApiCall(string apiPath, HttpContent httpContent, int methodType)
        {
            var httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(Properties.Settings.Default.ApiUri);
            httpClient.DefaultRequestHeaders.Accept.Clear();
            httpClient.DefaultRequestHeaders.Accept.Add(MediaTypeWithQualityHeaderValue.Parse("application/json"));
            if (_signinmodel != null)
            {
                httpClient.DefaultRequestHeaders.Authorization =
                    new AuthenticationHeaderValue("Bearer", _signinmodel.User.ApiToken);
            }
            HttpResponseMessage httpResponseMessage;

            switch (methodType)
            {
                case 1:
                    httpResponseMessage = httpClient.GetAsync(apiPath).Result;
                    string response = httpResponseMessage.Content.ReadAsStringAsync().Result;
                    return response;

                case 2:
                    httpResponseMessage = httpClient.PostAsync(apiPath, httpContent).Result;
                    return httpResponseMessage.Content.ReadAsStringAsync().Result;

                case 3:
                    httpResponseMessage = httpClient.DeleteAsync(apiPath).Result;
                    return httpResponseMessage.Content.ReadAsStringAsync().Result;
                case 4:
                    //httpResponseMessage = PatchAsync(httpClient, apiPath, httpContent);
                    var responseMessage = PatchAsync(httpClient, apiPath, httpContent);
                    return responseMessage.Result.Content.ReadAsStringAsync().Result; // httpResponseMessage.Content.ReadAsStringAsync().Result;

                default:
                    return null;

            }

        }

    }

    public struct MethodType
    {
        public static int get { get { return 1; } }
        public static int post { get { return 2; } }
        public static int delete { get { return 3; } }
        public static int patch { get { return 4; } }

    }
}

