﻿using Newtonsoft.Json;
using Project_Prototype_v2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Project_Prototype_v2

{
    public class SignupApi
    {
        public SignupApi()
        {

        }
        /*This is where the input data will be put in and send to our SignupUserModel*/
        public SignupModel Signup(
            string name,
            string email,
            string password,
            string password_confirmation,
            string invite_code
        )
        {
            /*Convert our input data to json*/
            var JsonData = JsonConvert.SerializeObject(
                new
                {
                    name = name,
                    email = email,
                    password = password,
                    password_confirmation = password_confirmation,
                    invite_code = invite_code
                });
            var httpContent = new ByteArrayContent(Encoding.UTF8.GetBytes(JsonData));
            /*Put our headers*/
            httpContent.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");

            var httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(Project_Prototype_v2.Properties.Settings.Default.ApiUri);
            httpClient.DefaultRequestHeaders.Accept.Clear();
            httpClient.DefaultRequestHeaders.Accept.Add(MediaTypeWithQualityHeaderValue.Parse("application/json"));
            /*The URI we use for the create user request*/
            var httpResponseMessage = httpClient.PostAsync("/api/v2/users", httpContent).Result;
            /*The status code we want to read when a request happened or failed*/
            var statusCode = httpResponseMessage.StatusCode;
            var response = httpResponseMessage.Content.ReadAsStringAsync().Result;
            System.Diagnostics.Debug.WriteLine(response);

            var signupmodel = JsonConvert.DeserializeObject<SignupModel>(response);
            /*If the status code we get is 'created' then we know the register worked
             *If we didnt get that exact code we know something went wrong and we want to know what error occured
             */
            if (statusCode == System.Net.HttpStatusCode.Created)
            {
                return signupmodel;
            }
            else
            {
                Console.WriteLine("Register failed: " + statusCode);
                throw new Exception("Signup failed: " + statusCode + ", " + signupmodel.Message);
            }
        }

    }
}
