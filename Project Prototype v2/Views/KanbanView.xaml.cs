﻿using Newtonsoft.Json;
using Project_Prototype_v2.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Project_Prototype_v2.Views
{
    /// <summary>
    /// Interaction logic for KanbanView.xaml
    /// </summary>
    public partial class KanbanView : Page
    {
        private ObservableCollection<BacklogItemModel> BacklogData = new ObservableCollection<BacklogItemModel>();
        private ObservableCollection<BacklogItemModel> InProgressData = new ObservableCollection<BacklogItemModel>();
        private ObservableCollection<BacklogItemModel> DoneData = new ObservableCollection<BacklogItemModel>();

        public ObservableCollection<BacklogItemModel> backlogData
        {
            get
            {
                return BacklogData;
            }
        }

        public ObservableCollection<BacklogItemModel> inProgressData
        {
            get
            {
                return InProgressData;
            }
        }

        public ObservableCollection<BacklogItemModel> doneData
        {
            get
            {
                return DoneData;
            }
        }

        Api _api = new Api();

        public KanbanView()
        {
            var KanbanBacklogItems =
                JsonConvert.DeserializeObject<KanbansModel>(
                    _api.ApiCall("/api/v2/project/" + ProjectsView.projectSlug + "/kanban/" + SprintsView.sprintSlug + "/list", null, MethodType.get)
                    );

            foreach (var backlogitem in KanbanBacklogItems.Kanban.Backlog)
            {
                BacklogData.Add(backlogitem);
            }

            backlogData.CollectionChanged += KanbanOnCollectionChanged;

            foreach (var backlogitem in KanbanBacklogItems.Kanban.InProgress)
            {
                InProgressData.Add(backlogitem);
            }

            inProgressData.CollectionChanged += KanbanOnCollectionChanged;

            foreach (var backlogitem in KanbanBacklogItems.Kanban.Done)
            {
                DoneData.Add(backlogitem);
            }

            doneData.CollectionChanged += KanbanOnCollectionChanged;

            this.DataContext = this;

            InitializeComponent();
        }

        private void KanbanOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {

        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
