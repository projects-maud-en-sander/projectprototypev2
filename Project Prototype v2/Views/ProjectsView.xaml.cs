﻿using Newtonsoft.Json;
using Project_Prototype_v2.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Project_Prototype_v2.Views
{
    /// <summary>
    /// Interaction logic for ProjectsView.xaml
    /// </summary>
    public partial class ProjectsView : Page
    {
        private ObservableCollection<ProjectModel> ProjectsData = new ObservableCollection<ProjectModel>();

        public ObservableCollection<ProjectModel> projectsData
        {
            get { return ProjectsData; }
        }

        //call API
        Api _api = new Api();
        MessageHandler _msg = new MessageHandler();

        //make static variable so that it can be used in SprintsView
        public static string projectSlug;

        public static bool ifOwnprojects;

        public ProjectsView()
        {
            
            //Get all the projects with the GET method
            var AllProjects = JsonConvert.DeserializeObject<ProjectsModel>(_api.ApiCall("/api/v2/projects", null, MethodType.get));

            if (ifOwnprojects)
            {
                //put all projects in the observerable collection
                foreach (var project in AllProjects.OwnProjects)
                {
                    ProjectsData.Add(project);
                }
            }
            else
            {
                foreach (var project in AllProjects.Projects)
                {
                    ProjectsData.Add(project);
                }
            }

            //if the collection is changed call function
            projectsData.CollectionChanged += ProjectsOnCollectionChanged;

            //send all the data to the front so it can be used for Binding
            this.DataContext = this;

            InitializeComponent();
        }

        private void ProjectsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        /// <summary>
        /// Calling the SprintView
        /// </summary>
        private void SprintsView(object sender, RoutedEventArgs e)
        {
            //get the selected project and assign it to the model
            var project = projectsGird.SelectedItem as ProjectModel;

            if (project != null)
            {
                //get the project slug
                projectSlug = project.Slug;

                //load the SprintView
                if (Application.Current.Windows
                    .Cast<Window>()
                    .FirstOrDefault(window => window is MainWindow) is MainWindow mw) mw.Main.Content = new SprintsView();
            }
            else
            {
                _msg.ErrorMessage("Je hebt geen project geselecteerd", messageBox, message);
            }
            
        }

        private void BacklogItemsView(object sender, RoutedEventArgs e)
        {
            var project = projectsGird.SelectedItem as ProjectModel;

            if (project != null)
            {
                projectSlug = project.Slug;

                if (Application.Current.Windows
                    .Cast<Window>()
                    .FirstOrDefault(window => window is MainWindow) is MainWindow mw)
                    mw.Main.Content = new BacklogItemsView();
            }
            else
            {
                _msg.ErrorMessage("Je hebt geen project geselecteerd", messageBox, message);
            }
        }

        private void DeleteProject(object sender, RoutedEventArgs e)
        {
            //get the selected project and assign it to the model
            var project = projectsGird.SelectedItem as ProjectModel;

            string resp_str;

            if (project != null)
            {
                resp_str = _api.ApiCall("/api/v2/projects/" + project.Slug, null, MethodType.delete);
            }
            else
            {
                _msg.ErrorMessage("Je hebt geen project geselecteerd", messageBox, message);
                return;
            }

            if (resp_str.Contains("OK"))
            {
                 if (Application.Current.Windows
                    .Cast<Window>()
                    .FirstOrDefault(window => window is MainWindow) is MainWindow mw) mw.Main.Content = new ProjectsView();
            }
            else
            {
                _msg.ErrorMessage("Dit is niet jouw project", messageBox, message);
            }

            //JsonConvert.DeserializeObject<ProjectModel>(_api.ApiCall("/api/v2/projects/"+project.Slug, null, MethodType.delete));
        }

        private void Onchecked(object sender, RoutedEventArgs e)
        {
            if (ownprojects.IsChecked == null)
                return;
            if ((bool)ownprojects.IsChecked)
            {
                ifOwnprojects = true;
                
            }
            else
            {
                ifOwnprojects = false;
            }

            Show();
        }

        private void Show()
        {
            //Get all the projects with the GET method
            ProjectsData.Clear();

            var AllProjects = JsonConvert.DeserializeObject<ProjectsModel>(_api.ApiCall("/api/v2/projects", null, MethodType.get));

            if (ifOwnprojects)
            {
                //put all projects in the observerable collection
                foreach (var project in AllProjects.OwnProjects)
                {
                    ProjectsData.Add(project);
                }
            }
            else
            {
                foreach (var project in AllProjects.Projects)
                {
                    ProjectsData.Add(project);
                }
            }
            
            this.DataContext = this;

            searchBoxTextChanged(null, null);
        }

        private void EditProject(object sender, RoutedEventArgs e)
        {
            var project = projectsGird.SelectedItem as ProjectModel;

            if (project != null)
            {
                if (Application.Current.Windows
                    .Cast<Window>()
                    .FirstOrDefault(window => window is MainWindow) is MainWindow mw) mw.Main.Content = new EditProjectView(project);
            }
            else
            {
                _msg.ErrorMessage("Je hebt geen project geselecteerd", messageBox, message);
            }
        }

        private void searchBoxTextChanged(object sender, TextChangedEventArgs e)
        {
            if(search != null && projectsGird != null) {
                if(search.Text == "Zoeken" || search.Text == "")
                {
                    projectsGird.ItemsSource = ProjectsData;
                }
                else
                {
                    search.Text.ToLower();
                    var filtered = ProjectsData.Where(project => project.Title.Contains(search.Text));
                    projectsGird.ItemsSource = filtered;
                }
            }
        }

        public EditProjectView EditProjectView
        {
            get => default;
            set
            {
            }
        }
    }
}
