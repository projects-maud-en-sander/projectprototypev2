﻿using Newtonsoft.Json;
using Project_Prototype_v2.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Project_Prototype_v2.Views
{
    /// <summary>
    /// Interaction logic for EditProjectView.xaml
    /// </summary>
    public partial class EditProjectView : Page
    {
        private ObservableCollection<ProjectModel> ProjectsData = new ObservableCollection<ProjectModel>();

        public ObservableCollection<ProjectModel> projectsData
        {
            get { return ProjectsData; }
        }

        private ObservableCollection<UserModel> UsersData = new ObservableCollection<UserModel>();

        public ObservableCollection<UserModel> usersData
        {
            get { return UsersData; }
        }

        Api _api = new Api();
        private readonly MessageHandler _msg = new MessageHandler();

        private ProjectModel project;

        public EditProjectView(ProjectModel project)
        {
            ProjectsData.Add(project); //this.project = project;

            foreach (var user in GetUsers())
            {
                UsersData.Add(user);
            }

            this.project = project;
            DataContext = this;
            InitializeComponent();
        }

        public UserModel[] GetUsers()
        {
            return JsonConvert.DeserializeObject<UsersModel>(_api.ApiCall("/api/v2/users", null, MethodType.get)).Users;
        }

        public string projectDescription;
        public string projectDeadline;
        public IList projectUsers;
        private void EditPrject(object sender, RoutedEventArgs e)
        {
            projectDescription = description.Text;
            projectDeadline = deadline.SelectedDate?.ToString("yyyy-M-dd");
            projectUsers = users.SelectedItems;
            
            long[] userIds = new long[users.SelectedItems.Count];
            for (int i = 0; i < users.SelectedItems.Count; i++)
            {
                var userAsModel = users.SelectedItems[i] as UserModel;
                userIds[i] = userAsModel.Id;
            }

            var JsonDate = JsonConvert.SerializeObject(
               new
               {
                   description = projectDescription,
                   deadline = projectDeadline,
                   users = userIds
               });
            var httpContent = new ByteArrayContent(Encoding.UTF8.GetBytes(JsonDate));
            httpContent.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");
            var resp_str = _api.ApiCall("/api/v2/projects/" + project.Slug, httpContent, MethodType.patch);

            if (resp_str.Contains("OK"))
            {
                JsonConvert.DeserializeObject<ProjectsModel>(resp_str);

                _msg.SuccessMessage("Het project is gewijzigd!", messageBox, message);
            }
            else if (resp_str.Contains("401"))
            {
                _msg.ErrorMessage("Jij bent geen deelnemer aan dit project", messageBox, message);
            }
            else
            {
                _msg.ErrorMessage("Voer geldige waardes in", messageBox, message);
            }
            //JsonConvert.DeserializeObject<ProjectsModel>(_api.ApiCall("/api/v2/projects/"+project.Slug, httpContent, MethodType.patch));
        }
    }
}
