﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Project_Prototype_v2.Views
{
    /// <summary>
    /// Interaction logic for LoginView.xaml
    /// </summary>
    public partial class LoginView : Window
    {
        public System.Windows.Forms.NotifyIcon ni = new System.Windows.Forms.NotifyIcon();
        public LoginView()
        {
            InitializeComponent();

            ni.Icon = new System.Drawing.Icon("logo.ico");
            ni.Visible = true;
            ni.DoubleClick +=
                delegate (object sender, EventArgs args)
                {
                    //this.Show();
                    this.WindowState = WindowState.Normal;
                };

            emailTexbox.Text = Properties.Settings.Default.Email;
            passwordTextbox.Password = Properties.Settings.Default.Password;
            if (Properties.Settings.Default.Email != "")
            {
                remember_me.IsChecked = true;
            }
        }


        public static string email;
        public static string password;


        public void LoginButton_Click(object sender, RoutedEventArgs e)
        {

            if (String.IsNullOrEmpty(emailTexbox.Text) == false && String.IsNullOrEmpty(passwordTextbox.Password) == false)
            {
                email = emailTexbox.Text;
                password = passwordTextbox.Password;

                Api _repository = new Api();

                if (_repository._signinmodel.User != null)
                {
                    if (remember_me.IsChecked == true)
                    {
                        Properties.Settings.Default.Email = email;
                        Properties.Settings.Default.Password = password;
                        Properties.Settings.Default.Save();
                    }
                    else
                    {
                        Properties.Settings.Default.Email = null;
                        Properties.Settings.Default.Password = null;
                        Properties.Settings.Default.Save();
                    }

                    //MessageBox.Show("Welkom terug " + _repository._signinmodel.User.Name);
                    MainWindow _mainWindow = new MainWindow();
                    _mainWindow.Topmost = true;
                    _mainWindow.Show();
                    ni.Icon = null;
                    this.Close();
                }
                else
                {
                    MessageBox.Show("De inloggegevens zijn verkeerd");
                }

            }
            else
            {
                MessageBox.Show("De velden mogen niet leeg zijn");
            }


        }


        protected override void OnStateChanged(EventArgs e)
        {
            if (WindowState == WindowState.Minimized)
            {
                this.Hide();
            }
            base.OnStateChanged(e);

        }

        private void emailTexbox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void rgisterView(object sender, RoutedEventArgs e)
        {
            SignupView _signupView = new SignupView();
            _signupView.Topmost = true;
            _signupView.Show();
            ni.Icon = null;
            this.Close();
        }
    }
}

