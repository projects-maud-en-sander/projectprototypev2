﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScrumApp_Prototype.ViewModels
{

    public class SignupViewModel : INotifyPropertyChanged
    {

        public string Error { get { return null; } }

        

        public string this[string name,string email, string password, string confirm_password, string invite_code]
        {
            get
            {
                string result = null;

                switch (name)
                {
                    case "name":
                        if (string.IsNullOrWhiteSpace(name))
                            result = "name cannot be empty";
                        else if (name.Length < 5)
                            result = "name must be a minimum of 5 characters.";
                        break;
                }
                switch (email)
                {
                    case "email":
                        if (string.IsNullOrWhiteSpace(email))
                            result = "Email cannot be empty";
                        else if (email.Length < 5)
                            result = "Email must be a minimum of 5 characters.";
                        break;
                }
                switch (password)
                {
                    case "password":
                        if (string.IsNullOrWhiteSpace(password))
                            result = "password cannot be empty";
                        else if (password.Length < 5)
                            result = "password must be a minimum of 5 characters.";
                        break;
                }
                switch (confirm_password)
                {
                    case "confirm_password":
                        if (string.IsNullOrWhiteSpace(confirm_password))
                            result = "confirm_password cannot be empty";
                        else if (confirm_password.Length < 5)
                            result = "confirm_password must be a minimum of 5 characters.";
                        break;
                }
                switch (invite_code)
                {
                    case "invite_code":
                        if (string.IsNullOrWhiteSpace(invite_code))
                            result = "invite_code cannot be empty";
                        else if (invite_code.Length < 5)
                            result = "invite_code must be a minimum of 5 characters.";
                        break;
                }

                return result; 
            }
        }

        

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
