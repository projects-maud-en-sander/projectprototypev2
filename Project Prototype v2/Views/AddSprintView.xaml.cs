﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;

namespace Project_Prototype_v2.Views
{
    /// <summary>
    /// Interaction logic for AddSprintView.xaml
    /// </summary>
    public partial class AddSprintView : Page
    {
        private readonly Api _api = new Api();
        private readonly MessageHandler _msg = new MessageHandler();

        public string SprintName;
        public string SprintStartDate;
        public string SprintEndDate;

        public AddSprintView()
        {
            InitializeComponent();
        }

        private void AddSprint(object sender, RoutedEventArgs e)
        {
            if (name.Text != "")
            {
                SprintName = name.Text;
            }
            else
            {
                _msg.ErrorMessage("Je hebt geen naam ingevoerd", messageBox, message);
                return;
            }

            if (startDate.Text != "")
            {
                SprintStartDate = startDate.SelectedDate?.ToString("yyyy-M-dd");
            } 
            else
            {
                _msg.ErrorMessage("Je hebt geen startdatum ingevoerd", messageBox, message);
                return;
            }

            if (endDate.Text != "")
            {
                SprintEndDate = endDate.SelectedDate?.ToString("yyyy-M-dd");
            }
            else
            {
                _msg.ErrorMessage("Je hebt geen einddatum ingevoerd", messageBox, message);
                return;
            }

            var JsonData = JsonConvert.SerializeObject(
                new
                {
                    name = SprintName,
                    start_date = SprintStartDate,
                    end_date = SprintEndDate,
                });

            var httpContent = new ByteArrayContent(Encoding.UTF8.GetBytes(JsonData));
            httpContent.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");
            var respString = _api.ApiCall("/api/v2/project/" + ProjectsView.projectSlug + "/sprint", httpContent, MethodType.post);

            if (respString.Contains("OK") == false)
            {
                _msg.ErrorMessage("Er is iets misgegaan", messageBox, message);
                return;
            }

            if (Application.Current.Windows
                .Cast<Window>()
                .FirstOrDefault(window => window is MainWindow) is MainWindow mw) mw.Main.Content = new SprintsView();
        }
    }
}
