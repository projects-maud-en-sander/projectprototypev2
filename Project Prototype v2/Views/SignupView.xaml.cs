﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Project_Prototype_v2.Views

{
    /// <summary>
    /// Interaction logic for SignupView.xaml
    /// </summary>
    public partial class SignupView : Window
    {
        private SignupApi _signupapi = new SignupApi();

        public SignupView()
        {
            InitializeComponent();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
            if (String.IsNullOrEmpty(name_input.Text) == true || String.IsNullOrEmpty(email_input.Text) == true || String.IsNullOrEmpty(passwordInput.Password) == true || String.IsNullOrEmpty(confirmpasswordIinput.Password) == true || String.IsNullOrEmpty(invite_code_input.Text) == true)
            {
                MessageBox.Show("Niet alles is ingevult, probeer opnieuw");
                return;
            }
            else
            {
                var name = name_input.Text;
                var email = email_input.Text;
                var password = passwordInput.Password;
                var confirm_password = confirmpasswordIinput.Password;
                var invite_code = invite_code_input.Text;
                Console.WriteLine("Register clicked");
                try
                {
                    var signupmodel = _signupapi.Signup(name, email, password, confirm_password, invite_code);
                    Console.WriteLine("Registreren voltooid: " + signupmodel.User.Id);
                    MessageBox.Show("Registreren voltooid: " + signupmodel.User.Id);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Registreren gefaald: " + ex);
                    MessageBox.Show("Registreren gefaald: " + ex.Message);
                }
                LoginView _LoginView = new LoginView();
                _LoginView.Show();
                this.Close();
            }

        }
        
        private void return_to_login_click(object sender, RoutedEventArgs e)
        {
            LoginView _LoginView = new LoginView();
            _LoginView.Show();
            this.Close();
        }
    }
}
        
