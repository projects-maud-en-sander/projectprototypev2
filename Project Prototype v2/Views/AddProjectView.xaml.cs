﻿using Newtonsoft.Json;
using Project_Prototype_v2.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Project_Prototype_v2.Views
{
    /// <summary>
    /// Interaction logic for AddProjectView.xaml
    /// </summary>
    public partial class AddProjectView : Page
    {
        private ObservableCollection<UserModel> UsersData = new ObservableCollection<UserModel>();

        public ObservableCollection<UserModel> usersData
        {
            get { return UsersData; }
        }

        Api _api = new Api();
        MessageHandler _msg = new MessageHandler();

        public AddProjectView()
        {
            foreach (var user in GetUsers())
            {
                UsersData.Add(user);
            }

            DataContext = this;
            InitializeComponent();
        }

        public UserModel[] GetUsers()
        {
            return JsonConvert.DeserializeObject<UsersModel>(_api.ApiCall("/api/v2/users", null, MethodType.get)).Users;
        }

        public string projectTitle;
        public string projectDescription;
        public string projectDeadline;
        public IList projectUsers;

        private void AddProject(object sender, RoutedEventArgs e)
        {
            projectTitle = title.Text;
            projectDescription = description.Text;
            projectDeadline = deadline.SelectedDate?.ToString("yyyy-M-dd");
            projectUsers = users.SelectedItems;
            
            long[] userIds = new long[users.SelectedItems.Count];
            for (int i = 0; i < users.SelectedItems.Count; i++)
            {
                var userAsModel = users.SelectedItems[i] as UserModel;
                userIds[i] = userAsModel.Id;
            }
            
            var JsonData = JsonConvert.SerializeObject(
               new
               {
                   title = projectTitle,
                   description = projectDescription,
                   deadline = projectDeadline,
                   users = userIds
               });

            var httpContent = new ByteArrayContent(Encoding.UTF8.GetBytes(JsonData));
            httpContent.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");
            var resp_str = _api.ApiCall("/api/v2/projects", httpContent, MethodType.post);

            if(resp_str.Contains("OK"))
            {
                JsonConvert.DeserializeObject<ProjectsModel>(resp_str);

                _msg.SuccessMessage("Het project is toegevoegd!", messageBox, message);
            }
            else
            {
                _msg.ErrorMessage("Voer geldige waardes in", messageBox, message);
            }
        }
    }
}
