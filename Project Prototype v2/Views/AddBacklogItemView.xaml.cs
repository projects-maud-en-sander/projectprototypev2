﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;
using Project_Prototype_v2.Models;

namespace Project_Prototype_v2.Views
{
    /// <summary>
    /// Interaction logic for AddBacklogItemView.xaml
    /// </summary>
    public partial class AddBacklogItemView : Page
    {
        private readonly Api _api = new Api();
        private readonly MessageHandler _msg = new MessageHandler();

        public ObservableCollection<BacklogTypeModel> BacklogTypesData { get; } = new ObservableCollection<BacklogTypeModel>();

        public string UserRole;
        public string Action;
        public int StoryPoints;
        public long BacklogTypeId;

        public AddBacklogItemView()
        {
            foreach (var backlogType in GetBacklogTypes())
            {
                BacklogTypesData.Add(backlogType);
            }

            DataContext = this;

            InitializeComponent();
        }

        private void AddBacklogItem(object sender, RoutedEventArgs e)
        {
            if (user_role.Text != "")
            {
                UserRole = user_role.Text;
            }
            else
            {
                _msg.ErrorMessage("Je hebt geen rol ingevoerd", messageBox, message);
                return;
            }

            if (action.Text != "")
            {
                Action = action.Text;
            }
            else
            {
                _msg.ErrorMessage("Je hebt geen actie ingevoerd", messageBox, message);
                return;
            }

            var isInt = int.TryParse(story_points.Text, out StoryPoints);
            if (isInt == false)
            {
                _msg.ErrorMessage("Je hebt story points incorrect ingevoerd", messageBox, message);
                return;
            }

            if (backlogTypes.SelectedItem is BacklogTypeModel backlogType)
            {
                BacklogTypeId = backlogType.Id;
            }
            else
            {
                _msg.ErrorMessage("Je hebt geen backlog type geselecteerd", messageBox, message);
                return;
            }

            var jsonData = JsonConvert.SerializeObject(
                new
                {
                    user_role = UserRole,
                    action = Action,
                    story_points = StoryPoints,
                    backlog_type_id = BacklogTypeId,
                });

            var httpContent = new ByteArrayContent(Encoding.UTF8.GetBytes(jsonData));
            httpContent.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");
            JsonConvert.DeserializeObject<BacklogItemModel>(
                _api.ApiCall("/api/v2/project/" + ProjectsView.projectSlug + "/backlog_items", httpContent, MethodType.post));

            _msg.SuccessMessage("Backlog Item aangemaakt!", messageBox, message);
        }

        public BacklogTypeModel[] GetBacklogTypes()
        {
            return JsonConvert.DeserializeObject<BacklogTypesModel>(_api.ApiCall("/api/v2/backlogtypes", null, MethodType.get)).BacklogTypes;
        }

        private void ValidateIfInt(object sender, TextCompositionEventArgs e)
        {
            var regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
