﻿using Newtonsoft.Json;
using Project_Prototype_v2.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Project_Prototype_v2.Views
{
    /// <summary>
    /// Interaction logic for BacklogItemsView.xaml
    /// </summary>
    public partial class BacklogItemsView : Page
    {
        Api _api = new Api();
        MessageHandler _msg = new MessageHandler();

        private ObservableCollection<BacklogItemModel> BacklogItemsData = new ObservableCollection<BacklogItemModel>();

        public ObservableCollection<BacklogItemModel> backlogItemsData
        {
            get { return BacklogItemsData; }
        }

        public EditBacklogItemView EditBacklogItemView
        {
            get => default;
            set
            {
            }
        }

        public BacklogItemsView()
        {
            var allBacklogItems =
                JsonConvert.DeserializeObject<BacklogItemsModel>(
                    _api.ApiCall("/api/v2/project/" + ProjectsView.projectSlug + "/backlog_items", null, MethodType.get)
                    );

            foreach (var backlogitem in allBacklogItems.BacklogItems)
            {
                BacklogItemsData.Add(backlogitem);
            }

            backlogItemsData.CollectionChanged += BacklogItemsOnCollectionChanged;

            DataContext = this;

            InitializeComponent();
        }

        private void AddBacklogItemView(object sender, RoutedEventArgs e)
        {
            if (Application.Current.Windows
                .Cast<Window>()
                .FirstOrDefault(window => window is MainWindow) is MainWindow mw) mw.Main.Content = new AddBacklogItemView();
        }

        private void EditBacklogItem(object sender, RoutedEventArgs e)
        {
            var backlogItem = backlogItemsGrid.SelectedItem as BacklogItemModel;

            if (backlogItem != null)
            {
                if (Application.Current.Windows
                    .Cast<Window>()
                    .FirstOrDefault(window => window is MainWindow) is MainWindow mw) mw.Main.Content = new EditBacklogItemView(backlogItem);
            }
            else
            {
                _msg.ErrorMessage("Je hebt geen backlog item geselecteerd", messageBox, message);
            }
        }

        private void DeleteBacklogItem(object sender, RoutedEventArgs e)
        {
            var backlogItem = backlogItemsGrid.SelectedItem as BacklogItemModel;

            if (backlogItem != null)
            {
                _api.ApiCall("/api/v2/project/" + ProjectsView.projectSlug + "/backlog_items/" + backlogItem.Id, null,
                    MethodType.delete);

                if (Application.Current.Windows
                    .Cast<Window>()
                    .FirstOrDefault(window => window is MainWindow) is MainWindow mw)
                    mw.Main.Content = new BacklogItemsView();
            }
            else
            {
                _msg.ErrorMessage("Je hebt geen backlog item geselecteerd", messageBox, message);
            }
        }

        private void FilterUserRole(object sender, KeyEventArgs e)
        {
            var filteredBacklogItems = backlogItemsData.Where(backlogItem => backlogItem.UserRole.Contains(FilterUserRoleTextBox.Text));

            backlogItemsGrid.ItemsSource = filteredBacklogItems;
        }

        private void FilterAction(object sender, KeyEventArgs e)
        {
            var filteredBacklogItems = backlogItemsData.Where(backlogItem => backlogItem.Action.Contains(FilterActionTextBox.Text));

            backlogItemsGrid.ItemsSource = filteredBacklogItems;
        }

        private void BacklogItemsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {

        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
