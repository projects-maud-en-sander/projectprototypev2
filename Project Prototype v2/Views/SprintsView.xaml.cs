﻿using Newtonsoft.Json;
using Project_Prototype_v2.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Project_Prototype_v2.Views
{
    /// <summary>
    /// Interaction logic for SprintsView.xaml
    /// </summary>
    public partial class SprintsView : Page
    {
        private ObservableCollection<SprintModel> SprintsData = new ObservableCollection<SprintModel>();

        public ObservableCollection<SprintModel> sprintsData
        {
            get { return SprintsData; }
        }

        //call API
        Api _api = new Api();
        MessageHandler _msg = new MessageHandler();

        public static string sprintSlug;

        public SprintsView()
        {
            //Get all the sprints with the GET method and static variable projectSlug
            var AllSprints = JsonConvert.DeserializeObject<SprintsModel>(_api.ApiCall("/api/v2/project/"+ProjectsView.projectSlug+"/sprint", null, MethodType.get));

            //put all sprints in the observerable collection
            foreach (var sprint in AllSprints.Sprints)
            {
                SprintsData.Add(sprint);
            }

            //if the collection is changed call function
            sprintsData.CollectionChanged += SprintsOnCollectionChanged;

            //send all the data to the front so it can be used for Binding
            this.DataContext = this;

            InitializeComponent();
        }

        private void AddSprintView(object sender, RoutedEventArgs e)
        {
            if (Application.Current.Windows
                .Cast<Window>()
                .FirstOrDefault(window => window is MainWindow) is MainWindow mw) mw.Main.Content = new AddSprintView();
        }

        private void DeleteSprint(object sender, RoutedEventArgs e)
        {
            var sprint = sprintsGrid.SelectedItem as SprintModel;

            if (sprint != null)
            {
                _api.ApiCall("api/v2/project/" + ProjectsView.projectSlug + "/sprint/" + sprint.Slug, null, MethodType.delete);

                if (Application.Current.Windows
                    .Cast<Window>()
                    .FirstOrDefault(window => window is MainWindow) is MainWindow mw) mw.Main.Content = new SprintsView();
            }
            else
            {
                _msg.ErrorMessage("Je hebt geen sprint geselecteerd", messageBox, message);
            }
        }

        private void ShowKanban(object sender, RoutedEventArgs e)
        {
            var sprint = sprintsGrid.SelectedItem as SprintModel;

            if (sprint != null)
            {
                sprintSlug = sprint.Slug;

                if (Application.Current.Windows
                    .Cast<Window>()
                    .FirstOrDefault(window => window is MainWindow) is MainWindow mw) mw.Main.Content = new KanbanView();
            }
            else
            {
                _msg.ErrorMessage("Je hebt geen sprint geselecteerd", messageBox, message);
            }
        }

        private void SprintsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {

        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
