﻿using Project_Prototype_v2.Models;
using Project_Prototype_v2.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Project_Prototype_v2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public System.Windows.Forms.NotifyIcon ni = new System.Windows.Forms.NotifyIcon();
        public MainWindow()
        {


            InitializeComponent();
            ni.Icon = new System.Drawing.Icon("logo.ico");
            ni.Visible = true;
            ni.DoubleClick +=
                delegate (object sender, EventArgs args)
                {
                    //this.Show();
                    this.WindowState = WindowState.Normal;
                };
        }

        private void ProjectView(object sender, RoutedEventArgs e)
        {

            if (Application.Current.Windows
                .Cast<Window>()
                .FirstOrDefault(window => window is MainWindow) is MainWindow mw) mw.Main.Content = new ProjectsView();
        }

        private void AddProjectView(object sender, RoutedEventArgs e)
        {

            if (Application.Current.Windows
                .Cast<Window>()
                .FirstOrDefault(window => window is MainWindow) is MainWindow mw) mw.Main.Content = new AddProjectView();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            LoginView _loginView = new LoginView();
            _loginView.Show();
            ni.Icon = null;
            this.Close();
        }
    }
}
