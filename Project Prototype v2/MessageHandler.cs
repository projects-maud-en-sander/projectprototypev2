﻿using System.Drawing;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Brush = System.Windows.Media.Brush;
using Color = System.Windows.Media.Color;
using Rectangle = System.Windows.Shapes.Rectangle;

namespace Project_Prototype_v2
{
    public class MessageHandler
    {
        public void ErrorMessage(string messageText, Rectangle messageBox, TextBlock message)
        {
            SolidColorBrush mySolidColorBrush = new SolidColorBrush
            {
                Color = Color.FromRgb(255, 0, 0)
            };

            messageBox.Fill = mySolidColorBrush;
            messageBox.Visibility = Visibility.Visible;
            messageBox.OpacityMask = (Brush)(new BrushConverter().ConvertFrom("#7F000000"));

            message.Visibility = Visibility.Visible;
            message.Text = messageText;
            message.FontWeight = FontWeights.Bold;
            message.TextAlignment = TextAlignment.Center;
        }

        public void SuccessMessage(string messageText, Rectangle messageBox, TextBlock message)
        {
            var mySolidColorBrush = new SolidColorBrush
            {
                Color = Color.FromRgb(50, 205, 50)
            };

            messageBox.Fill = mySolidColorBrush;
            messageBox.Visibility = Visibility.Visible;
            messageBox.OpacityMask = (Brush)(new BrushConverter().ConvertFrom("#7F000000"));

            message.Visibility = Visibility.Visible;
            message.Text = messageText;
            message.FontWeight = FontWeights.Bold;
            message.TextAlignment = TextAlignment.Center;
        }
    }
}