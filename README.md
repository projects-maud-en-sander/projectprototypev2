﻿# Prototype Scrum App

This is the installation process for our new Prototype Scrum App!


# Installation

 - Download the Prototype_ScrumApp.zip to your machine.
 - Navigate to \Prototype\Release.
 - Double click the setup.exe to start the installation process.
 - Follow the setup wizard with the wanted settings.
 - The application is installed!

# Dependencies
- MaterialDesignColors.1.2.2
- MaterialDesignThemes.3.0.1
- Newtonsoft.Json.12.0.3
- ShowMeTheXAML.1.0.12
- ShowMeTheXAML.MSBuild.1.0.12

# Version
2.0

# API reference
[http://sad.clow.nl/](http://sad.clow.nl/)


# Questions
Email: s1146976@student.windesheim.nl



